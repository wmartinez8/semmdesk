﻿namespace SemmDesk.BLL
{
    using SemmDesk.DAL.Contexto;
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class RolBLL : ICRUD<Role>
    {
        /*
          No implementado porque los roles no tendran un mantenimiento. son pocos y una vez creados en la BD no creo que necesiten
          un mantenimiento
       */
        private readonly RolDAL _rolDAL;
        public RolBLL()
        {
            this._rolDAL = new RolDAL();
        }

        public List<Role> GetAll()
        {
            var lsRoles = _rolDAL.GetAll().OrderBy(x => x.Descripcion).ToList();            
            return lsRoles;
        }

        public Role GetById(int id)
        {
            Role obj = _rolDAL.GetById(id);
            return obj;
        }

        public void Insert(Role obj)
        {
            _rolDAL.Insert(obj);
        }

        public void Update(Role obj)
        {
            _rolDAL.Update(obj);
        }

        public void Delete(int id)
        {
            _rolDAL.Delete(id);
        }
    }
}
