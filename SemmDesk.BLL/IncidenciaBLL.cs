﻿namespace SemmDesk.BLL
{
    using SemmDesk.DAL.Contexto;
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class IncidenciaBLL : ICRUD<Incidencia>
    {
        private readonly IncidenciasDAL _incidencidaDAL;

        public IncidenciaBLL()
        {
            this._incidencidaDAL = new IncidenciasDAL();
        }

        public List<Incidencia> GetAll()
        {
            var lsIncidencias =  _incidencidaDAL.GetAll().OrderBy(x => x.IncidenciaId).ToList();             
            return lsIncidencias;
        }

        //public IEnumerable<Incidencia> GetAllByUsuarioId(int usuarioId)
        //{
        //    IEnumerable<Incidencia> lsIncidencias = _incidencidaDAL.GetAllByUsuarioId(usuarioId);
        //    return lsIncidencias;
        //}

        public  Incidencia GetById(int id)
        {
            Incidencia obj = _incidencidaDAL.GetById(id);
            return obj;
        }

        public void Insert(Incidencia obj)
        {
            obj.FechaCreacion = DateTime.Now;
            if (obj.PrioridadID == 0)
            {
                obj.PrioridadID = 3;
            }
            if (obj.EstadoIncidenciaID == 0)
            {
                obj.EstadoIncidenciaID = 1;
            }
            if (obj.TecnicoID == 0)
            {
                obj.TecnicoID = 2;
            }
            if (obj.EstadoIncidenciaID == 3)
            {
                obj.FechaCierre = DateTime.Now;
            }

            _incidencidaDAL.Insert(obj);
        }

        public void Update(Incidencia obj)
        {
            obj.UltimaModificacion = DateTime.Now;

            if (obj.FechaCierre == null)
            {
                if (obj.EstadoIncidenciaID == 3)
                {
                    obj.FechaCierre = DateTime.Now;
                }
            }
            _incidencidaDAL.Update(obj);
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
