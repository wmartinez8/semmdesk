﻿namespace SemmDesk.BLL
{
    using SemmDesk.DAL.Contexto;
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class EstadoIncidenciaBLL : ICRUD<EstadoIncidencia>
    {
        /*
         * NO TENDRAN UN MANTENIMIENTO
         */
        private readonly EstadoIncidenciaDAL _estadoIncidenciaDAL;

        public EstadoIncidenciaBLL()
        {
            this._estadoIncidenciaDAL = new EstadoIncidenciaDAL();
        }

        public List<EstadoIncidencia> GetAll()
        {
            var lsEstados =  _estadoIncidenciaDAL.GetAll().OrderBy(x => x.Descripcion).ToList();
            return lsEstados;
        }

        public EstadoIncidencia GetById(int id)
        {
            EstadoIncidencia obj = _estadoIncidenciaDAL.GetById(id);
            return obj;
        }

        public void Insert(EstadoIncidencia obj)
        {
            _estadoIncidenciaDAL.Insert(obj);
        }

        public void Update(EstadoIncidencia obj)
        {

            _estadoIncidenciaDAL.Update(obj);
        }

        public void Delete(int id)
        {
            _estadoIncidenciaDAL.Delete(id);
        }


    }
}
