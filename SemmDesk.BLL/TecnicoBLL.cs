﻿namespace SemmDesk.BLL
{
    using SemmDesk.DAL.Contexto;
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class TecnicoBLL : ICRUD<Tecnico>
    {
        private readonly TecnicoDAL _tecnicoDAL;

        public TecnicoBLL()
        {
            this._tecnicoDAL = new TecnicoDAL();
        }

        public List<Tecnico> GetAll()
        {
            var lsTecnicos = _tecnicoDAL.GetAll().OrderBy(x => x.Nombres).ToList();            
            return lsTecnicos;
        }

        public Tecnico GetById(int id)
        {
            Tecnico obj = _tecnicoDAL.GetById(id);
            return obj;
        }
        public Tecnico GetByByCredencial(Tecnico tecnico)
        {
            Tecnico obj = _tecnicoDAL.GetByByCredencial(tecnico);
            return obj;
        }
        public void Insert(Tecnico obj)
        {
            _tecnicoDAL.Insert(obj);
        }

        public void Update(Tecnico obj)
        {
            _tecnicoDAL.Update(obj);
        }

        public void Delete(int id)
        {
            _tecnicoDAL.Delete(id);
        }
    }
}
