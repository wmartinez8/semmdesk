﻿namespace SemmDesk.BLL
{
    using SemmDesk.DAL.Contexto;
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class DepartamentoBLL : ICRUD<vDepartamento>
    {
        /*
         * Los departamentos se llenaran desde la base de datos de CONSULTA. 
         * NO TENDRAN UN MANTENIMIENTO
         */
        private readonly DepartamentoDAL _departamentoDAL;
        public DepartamentoBLL()
        {
            this._departamentoDAL = new DepartamentoDAL();
        }

        public List<vDepartamento> GetAll()
        {
            var lsDepartamentos =  _departamentoDAL.GetAll().OrderBy(x => x.descrip).ToList();            

            return lsDepartamentos;
        }

        public vDepartamento GetById(int id)
        {
            vDepartamento obj = _departamentoDAL.GetById(id);
            return obj;
        }

        public void Insert(vDepartamento obj)
        {
            throw new NotImplementedException();
        }

        public void Update(vDepartamento obj)
        {
            throw new NotImplementedException();
        }
        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
