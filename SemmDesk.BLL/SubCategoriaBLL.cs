﻿namespace SemmDesk.BLL
{
    using SemmDesk.DAL.Contexto;
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class SubCategoriaBLL : ICRUD<SubCategoria>
    {
        private readonly SubCategoriaDAL _subcategoriaDAL;
        public SubCategoriaBLL()
        {
            this._subcategoriaDAL = new SubCategoriaDAL();
        }      

        public List<SubCategoria> GetAll()
        {
            var lsSubCategorias = _subcategoriaDAL.GetAll().OrderBy(x => x.Descripcion).ToList();            
            return lsSubCategorias;
        }

        public SubCategoria GetById(int id)
        {
            SubCategoria obj = _subcategoriaDAL.GetById(id);
            return obj;
        }

        public void Insert(SubCategoria obj)
        {
            _subcategoriaDAL.Insert(obj);
        }

        public void Update(SubCategoria obj)
        {
            _subcategoriaDAL.Update(obj);
        }
        public void Delete(int id)
        {
            _subcategoriaDAL.Delete(id);
        }
    }
}
