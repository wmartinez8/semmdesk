﻿namespace SemmDesk.BLL
{
    using SemmDesk.DAL.Contexto;
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    //TODO: ESTOY PUEDE SER UN ENUM? SOLO TENGO UNAS 3 PRIORIDADES
    public class PrioridadBLL : ICRUD<Prioridade>
    {
        /*
          NO TENDRAN UN MANTENIMIENTO
         */

        private readonly PrioridadDAL _prioridadDAL;
        public PrioridadBLL()
        {
            this._prioridadDAL = new PrioridadDAL();
        }

        public List<Prioridade> GetAll()
        {
            var lsPrioridades =  _prioridadDAL.GetAll().OrderBy(x => x.Descripcion).ToList();            
            return lsPrioridades;
        }

        public Prioridade GetById(int id)
        {
            Prioridade obj = _prioridadDAL.GetById(id);
            return obj;
        }

        public void Insert(Prioridade obj)
        {
            _prioridadDAL.Insert(obj);
        }

        public void Update(Prioridade obj)
        {
            _prioridadDAL.Update(obj);
        }

        public void Delete(int id)
        {
            _prioridadDAL.Delete(id);
        }
    }
}
