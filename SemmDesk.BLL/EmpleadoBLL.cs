﻿namespace SemmDesk.BLL
{
    using SemmDesk.DAL.Contexto;
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class EmpleadoBLL : ICRUD<Empleado>
    {
        private readonly EmpleadoDAL _empleadoDAL;
        public EmpleadoBLL()
        {
            this._empleadoDAL = new EmpleadoDAL();
        }

        public List<Empleado> GetAll()
        {
            var lsEmpleados = _empleadoDAL.GetAll().Where(x => x.Estatus != "E").OrderBy(x => x.Nombres).ToList();           

            return lsEmpleados;
        }

        public Empleado GetById(int id)
        {
            Empleado obj = _empleadoDAL.GetById(id);
            return obj;
        }
        public bool ChangePassword(Empleado empleado, string nuevaClave)
        {
            _empleadoDAL.ChangePassword(empleado, nuevaClave);
            return true;
        }
        public IEnumerable<Empleado> GetByDepartment(int DepartmentId)
        {
            var lsEmpleadosDepartamento = _empleadoDAL.GetAll().Where(x => x.DepartamentoId == DepartmentId 
                                                                      && x.Estatus != "E").ToList();
            return lsEmpleadosDepartamento;
        }
        public void Insert(Empleado obj)
        {
            _empleadoDAL.Insert(obj);
        }

        public void Update(Empleado obj)
        {
            _empleadoDAL.Update(obj);
        }

        public Empleado Login(int codigo)
        {
            Empleado obj = _empleadoDAL.Login(codigo);
            return obj;
        }

        public Empleado LoginWithCredentials(string clave)
        {
            Empleado obj = _empleadoDAL.LoginWithCredentials(clave);
            return obj;
        }

        public void Delete(int id)
        {
            _empleadoDAL.Delete(id);
        }

    }
}
