﻿namespace SemmDesk.BLL
{
    using SemmDesk.DAL.Contexto;
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class UsuarioBLL : ICRUD<vUsuario>
    {       
        private UsuarioDAL _usuarioDAL;
        public UsuarioBLL()
        {
            this._usuarioDAL = new UsuarioDAL();
        }
        public void Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<vUsuario> GetAll()
        {
            IEnumerable<vUsuario> obj = _usuarioDAL.GetAll();
            return obj;
        }

        public vUsuario GetById(int id)
        {
            vUsuario obj = _usuarioDAL.GetById(id);
         
            return obj;
        }

        public vUsuario Login(int codigo)
        {
            vUsuario obj =_usuarioDAL.Login(codigo);
            return obj;
        }

        public void Insert(vUsuario obj)
        {
            throw new System.NotImplementedException();
        }

        public void Update(vUsuario obj)
        {
            throw new System.NotImplementedException();
        }
    }
}
