﻿namespace SemmDesk.BLL
{
    using SemmDesk.DAL.Contexto;
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class CategoriaBLL : ICRUD<Categoria>
    {
        private readonly CategoriaDAL _categoriaDAL;
        public CategoriaBLL()
        {
            this._categoriaDAL = new CategoriaDAL();
        }               

        public  List<Categoria> GetAll()
        {   
            var lsOrdenada = _categoriaDAL.GetAll().OrderBy(x => x.Descripcion).ToList();

            return lsOrdenada;
        }

        public Categoria GetById(int id)
        {
            Categoria obj = _categoriaDAL.GetById(id);
            return obj;
        }

        public void Insert(Categoria obj)
        {
            //Aqui las validaciones de lugar antes de insertar los datos en la tabla
            _categoriaDAL.Insert(obj);

        }

        public void Update(Categoria obj)
        {
            _categoriaDAL.Update(obj);
        }

        public void Delete(int id)
        {
            //Aqui las reglas de validaciones para cada metodo y luego la funcion que hace el metodo. 
            // Por ejemplo las validaciones antes de mandar a eliminar y luego ejecuto
            _categoriaDAL.Delete(id);
        }
    }
}
