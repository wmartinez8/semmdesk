﻿namespace SemmDesk.DAL.Repositorios
{
    using SemmDesk.DAL.Interfaces;
    using System;
    using System.Linq;
    using System.Security.Cryptography;

    public class PasswordServices : IPasswordService
    {
        //TODO: usar los valores de las opciones del hash desde esta clase. Por ahora solo de prueba lo voy a poner quemados en el metodo
        //private readonly PasswordOptions _options;


        public bool Check(string hash, string password)
        {
            PasswordOptions passwordOptions = new PasswordOptions
            {
                SaltSize = 16,
                KeySize = 32,
                Iterations = 1
            };

            var parts = hash.Split(Convert.ToChar("."), Convert.ToChar(3));
            if (parts.Length != 3)
            {
                throw new FormatException("Unexpected hash format");
            }

            var iterations = Convert.ToInt32(parts[0]);
            var salt = Convert.FromBase64String(parts[1]);
            var key = Convert.FromBase64String(parts[2]);

            using (var algorithm = new Rfc2898DeriveBytes(
              password,
              salt,
              iterations,
              HashAlgorithmName.SHA512)
              )

            {
                var keyToCheck = algorithm.GetBytes(passwordOptions.KeySize);
                return keyToCheck.SequenceEqual(key);
            }
        }

        public string Hash(string password)
        {
            try
            {
                PasswordOptions passwordOptions = new PasswordOptions
                {
                    SaltSize = 16,
                    KeySize = 32,
                    Iterations = 10000
                };
                //PBKDF2 implementation. Search for more info
                using (var algorithm = new Rfc2898DeriveBytes(
                    password,
                    passwordOptions.SaltSize,
                    passwordOptions.Iterations,
                    HashAlgorithmName.SHA512)
                    )
                {
                    var key = Convert.ToBase64String(algorithm.GetBytes(passwordOptions.KeySize));
                    var salt = Convert.ToBase64String(algorithm.Salt);
                    return $"{passwordOptions.Iterations}.{salt}.{key}";
                }
            }
            catch (Exception)
            {

                throw new Exception("You must enter password parameter");
            }
        }
    }
}
