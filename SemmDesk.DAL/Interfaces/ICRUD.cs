﻿namespace SemmDesk.DAL.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    //TODO: volver estos metodos async
    public interface ICRUD<T> where T : class
    {
        List<T> GetAll();
        T GetById(int id);
        void Insert(T obj);
        void Update(T obj);
        void Delete(int id);
    }
}
