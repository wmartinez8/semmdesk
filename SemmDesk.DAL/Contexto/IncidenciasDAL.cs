﻿namespace SemmDesk.DAL.Contexto
{
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;

    public class IncidenciasDAL : ICRUD<Incidencia>
    {
        private readonly Modelo db = new Modelo();

        public List<Incidencia> GetAll()
        {
            var obj = db.Incidencias.ToList();
                                        //.Include("Prioridade")
                                        //.Include("EstadoIncidencia")
                                        //.Include("Tecnico")
                                        //.Include("Categoria")
                                        //.Include("SubCategoria")
                                        //.Include("Area")
                                        //.Include("Empleado")
                                        //.Include("Tecnico").ToList();
                                        //.Where(x => x.EmpleadoId == Empleado.Codigo).ToList();
            return obj;
        }

        public Incidencia GetById(int id)
        {
            Incidencia obj = db.Incidencias.Find(id);
            return obj;
        }

        //public IEnumerable<Incidencia> GetAllByUsuarioId(int UsuarioId)
        //{
        //    IEnumerable<Incidencia> lstIncidencias = db.Incidencias.ToList().Where(x => x.UsuarioID == UsuarioId);
        //    return lstIncidencias;
        //}

        public void Insert(Incidencia obj)
        {
            db.Incidencias.Add(obj);
            db.SaveChanges();
        }

        public void Update(Incidencia obj)
        {
            db.Set<Incidencia>().AddOrUpdate(obj);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
            //Incidencia obj = db.Incidencias.Find(id);
            //db.Incidencias.Remove(obj);
            //db.SaveChanges();
        }
    }
}
