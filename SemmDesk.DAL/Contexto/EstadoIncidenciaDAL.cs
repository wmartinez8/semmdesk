﻿namespace SemmDesk.DAL.Contexto
{
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;

    public class EstadoIncidenciaDAL : ICRUD<EstadoIncidencia>
    {
        private readonly Modelo db = new Modelo();

        public List<EstadoIncidencia> GetAll()
        {
            var obj = db.EstadoIncidencias.ToList();
            return obj;
        }

        public EstadoIncidencia GetById(int id)
        {
            EstadoIncidencia obj = db.EstadoIncidencias.Find(id);
            return obj;
        }

        public void Insert(EstadoIncidencia obj)
        {
            db.EstadoIncidencias.Add(obj);
            db.SaveChanges();
        }

        public void Update(EstadoIncidencia obj)
        {
            db.Set<EstadoIncidencia>().AddOrUpdate(obj);
            db.SaveChanges();
        }
        public void Delete(int id)
        {
            EstadoIncidencia obj = db.EstadoIncidencias.Find(id);
            db.EstadoIncidencias.Remove(obj);
            db.SaveChanges();
        }
    }
}
