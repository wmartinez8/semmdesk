﻿namespace SemmDesk.DAL.Contexto
{
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;

    public class TecnicoDAL : ICRUD<Tecnico>
    {
        private readonly Modelo db = new Modelo();

        public List<Tecnico> GetAll()
        {
            var obj = db.Tecnicos.ToList();
            return obj;
        }

        public Tecnico GetById(int id)
        {
            Tecnico obj = db.Tecnicos.FirstOrDefault(x => x.CodigoEmpleado == id);
            return obj;
        }

        public Tecnico GetByByCredencial(Tecnico tecnico)
        {
            Tecnico obj = db.Tecnicos.FirstOrDefault(
                                                     x => x.CodigoEmpleado == tecnico.CodigoEmpleado
                                                     && x.Clave == tecnico.Clave);
            return obj;
        }

        public void Insert(Tecnico obj)
        {
            db.Tecnicos.Add(obj);
            db.SaveChanges();
        }

        public void Update(Tecnico obj)
        {
            db.Set<Tecnico>().AddOrUpdate(obj);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            Tecnico obj = db.Tecnicos.Find(id);
            db.Tecnicos.Remove(obj);
            db.SaveChanges();
        }

    }
}
