﻿namespace SemmDesk.DAL.Contexto
{
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class UsuarioDAL : ICRUD<vUsuario>
    {      
        private Modelo db = new Modelo();

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<vUsuario> GetAll()
        {
            IEnumerable<vUsuario> lsUsuarios = db.vUsuarios.ToList();
            return lsUsuarios;
        }

        public vUsuario GetById(int id)
        {
            vUsuario obj = db.vUsuarios.FirstOrDefault(x => x.codigo == id);
            return obj;
        }

        public vUsuario Login(int codigo)
        {
            vUsuario obj = db.vUsuarios.FirstOrDefault(x => x.codigo == codigo); // .FirstOrDefault(x => x.co == codigo);
            return obj;
        }
        public void Insert(vUsuario obj)
        {
            throw new NotImplementedException();
        }

        public void Update(vUsuario obj)
        {
            throw new NotImplementedException();
        }
    }
}
