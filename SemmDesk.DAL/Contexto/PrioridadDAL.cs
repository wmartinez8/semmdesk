﻿namespace SemmDesk.DAL.Contexto
{
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;

    public class PrioridadDAL : ICRUD<Prioridade>
    {
        private readonly Modelo db = new Modelo();

        public List<Prioridade> GetAll()
        {
            var obj = db.Prioridades.ToList();
            return obj;
        }

        public Prioridade GetById(int id)
        {
            Prioridade obj = db.Prioridades.Find(id);
            return obj;
        }

        public void Insert(Prioridade obj)
        {
            db.Prioridades.Add(obj);
            db.SaveChanges();
        }

        public void Update(Prioridade obj)
        {
            db.Set<Prioridade>().AddOrUpdate(obj);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            Prioridade obj = db.Prioridades.Find(id);
            db.Prioridades.Remove(obj);
            db.SaveChanges();
        }
    }
}
