﻿namespace SemmDesk.DAL.Contexto
{
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;

    public class EmpleadoDAL : ICRUD<Empleado>
    {
        private readonly Modelo db = new Modelo();

        public List<Empleado> GetAll()
        {
            //TODO: MEJOR LOS DEMAS CODIGO DE LOS DALS COMO ESTE
            //OJO CUANDO USO EL AWAIT 
            var obj = db.Empleados.ToList();
            return obj;
        }

        public Empleado GetById(int id)
        {
            Empleado obj =  db.Empleados.FirstOrDefault(x => x.Codigo == id);
            return obj;
        }

        public bool ChangePassword(Empleado empleado, string nuevaClave)
        {
            try
            {
                empleado = GetById(empleado.EmpleadoId);
                empleado.Clave = nuevaClave;
                db.SaveChanges();
            }
            catch (System.Exception)
            {

                throw;
            }
            return true;
        }

        public void Insert(Empleado obj)
        {
            db.Empleados.Add(obj);
            db.SaveChanges();
        }

        public void Update(Empleado obj)
        {
            db.Set<Empleado>().AddOrUpdate(obj);
            db.SaveChanges();
        }

        public Empleado Login(int codigo)
        {
            Empleado obj = db.Empleados.FirstOrDefault(x => x.Codigo == codigo);
            return obj;
        }

        public Empleado LoginWithCredentials(string clave)
        {
            Empleado obj = db.Empleados.FirstOrDefault(x => x.Clave == clave);
            return obj;
        }

        public void Delete(int id)
        {
            Empleado obj = db.Empleados.Find(id);
            db.Empleados.Remove(obj);
            db.SaveChanges();
        }

    }
}
