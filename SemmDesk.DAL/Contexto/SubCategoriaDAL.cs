﻿namespace SemmDesk.DAL.Contexto
{
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;

    public class SubCategoriaDAL : ICRUD<SubCategoria>
    {
        private readonly Modelo db = new Modelo();

        public List<SubCategoria> GetAll()
        {
            var obj = db.SubCategorias.ToList();
            return obj;
        }

        public SubCategoria GetById(int id)
        {
            SubCategoria obj = db.SubCategorias.Find(id);
            return obj;
        }

        public void Insert(SubCategoria obj)
        {
            db.SubCategorias.Add(obj);
            db.SaveChanges();
        }

        public void Update(SubCategoria obj)
        {
            db.Set<SubCategoria>().AddOrUpdate(obj);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            SubCategoria obj = db.SubCategorias.Find(id);
            db.SubCategorias.Remove(obj);
            db.SaveChanges();
        }

    }
}
