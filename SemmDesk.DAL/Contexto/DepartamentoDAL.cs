﻿
namespace SemmDesk.DAL.Contexto
{
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class DepartamentoDAL : ICRUD<vDepartamento>
    {
        private readonly Modelo db = new Modelo();

        public List<vDepartamento> GetAll()
        {
            var obj = db.vDepartamentos.ToList();
            return obj;
        }

        public vDepartamento GetById(int id)
        {
            vDepartamento obj = db.vDepartamentos.Find(id);
            return obj;
        }

        public void Insert(vDepartamento obj)
        {
            throw new NotImplementedException();
        }

        public void Update(vDepartamento obj)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
