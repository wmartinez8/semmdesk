namespace SemmDesk.DAL.Contexto
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using SemmDesk.Entities.Models;

    public partial class Modelo : DbContext
    {
        public Modelo()
            : base("name=Modelo")
        {
        }

        public virtual DbSet<Categoria> Categorias { get; set; }
        public virtual DbSet<EstadoIncidencia> EstadoIncidencias { get; set; }
        public virtual DbSet<Incidencia> Incidencias { get; set; }
        public virtual DbSet<Menu> Menus { get; set; }
        public virtual DbSet<Prioridade> Prioridades { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<SubCategoria> SubCategorias { get; set; }
        public virtual DbSet<Empleado> Empleados { get; set; }
        public virtual DbSet<Tecnico> Tecnicos { get; set; }
        public virtual DbSet<vDepartamento> vDepartamentos { get; set; }
        public virtual DbSet<Area> Areas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Categoria>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Categoria>()
                .HasMany(e => e.Incidencias)
                .WithRequired(e => e.Categoria)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Categoria>()
                .HasMany(e => e.SubCategorias)
                .WithRequired(e => e.Categoria)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EstadoIncidencia>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<EstadoIncidencia>()
                .Property(e => e.Color)
                .IsUnicode(false);

            modelBuilder.Entity<Incidencia>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Incidencia>()
                .Property(e => e.ComentarioCierre)
                .IsUnicode(false);

            modelBuilder.Entity<Menu>()
                .Property(e => e.NombreMenu)
                .IsUnicode(false);

            modelBuilder.Entity<Menu>()
                .Property(e => e.ControllerName)
                .IsUnicode(false);

            modelBuilder.Entity<Menu>()
                .Property(e => e.ActionName)
                .IsUnicode(false);

            modelBuilder.Entity<Prioridade>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Prioridade>()
                .Property(e => e.Color)
                .IsUnicode(false);

            modelBuilder.Entity<Role>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<SubCategoria>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<SubCategoria>()
                .HasMany(e => e.Incidencias)
                .WithRequired(e => e.SubCategoria)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Tecnico>()
                .Property(e => e.Nombres)
                .IsUnicode(false);

            modelBuilder.Entity<Tecnico>()
                .Property(e => e.Identificador)
                .IsUnicode(false);

            modelBuilder.Entity<Tecnico>()
              .Property(e => e.CodigoEmpleado);
             


            modelBuilder.Entity<vDepartamento>()
                .Property(e => e.codito)
                .HasPrecision(6, 0);

            modelBuilder.Entity<vDepartamento>()
                .Property(e => e.descrip)
                .IsUnicode(false);

            modelBuilder.Entity<Empleado>()
              .Property(e => e.Estatus).IsUnicode(false);

            modelBuilder.Entity<Empleado>()
              .Property(e => e.Codigo);

            modelBuilder.Entity<Empleado>()
              .Property(e => e.Apellido1).IsUnicode(false);

            modelBuilder.Entity<Empleado>()
              .Property(e => e.Apellido2).IsUnicode(false);

            modelBuilder.Entity<Empleado>()
              .Property(e => e.Nombres).IsUnicode(false);

            modelBuilder.Entity<Empleado>()
              .Property(e => e.Sexo).IsUnicode(false);

            modelBuilder.Entity<Empleado>()
              .Property(e => e.DepartamentoId);

            modelBuilder.Entity<Empleado>()
              .Property(e => e.Telefono).IsUnicode(false);

            modelBuilder.Entity<Empleado>()
              .Property(e => e.Cargo).IsUnicode(false);

            modelBuilder.Entity<Empleado>()
              .Property(e => e.Foto).IsUnicode(false);

            modelBuilder.Entity<Empleado>()
              .Property(e => e.Departamento).IsUnicode(false);

            modelBuilder.Entity<Empleado>()
              .Property(e => e.Foto).IsUnicode(false);

            modelBuilder.Entity<Empleado>()
              .Property(e => e.FechaNacimiento);

            modelBuilder.Entity<Empleado>()
              .Property(e => e.EmpleadoId);

            modelBuilder.Entity<Empleado>()
              .Property(e => e.RolId);

            modelBuilder.Entity<Empleado>()
              .Property(e => e.Clave).IsUnicode(false);



            //modelBuilder.Entity<vUsuario>()
            //    .Property(e => e.status)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //modelBuilder.Entity<vUsuario>()
            //    .Property(e => e.Identificador)
            //    .IsUnicode(false);

            //modelBuilder.Entity<vUsuario>()
            //    .Property(e => e.Nombres)
            //    .IsUnicode(false);

            //modelBuilder.Entity<vUsuario>()
            //    .Property(e => e.departam)
            //    .HasPrecision(3, 0);

            //modelBuilder.Entity<vUsuario>()
            //    .Property(e => e.foto)
            //    .IsUnicode(false);

            //modelBuilder.Entity<vUsuario>()
            //    .Property(e => e.cargo)
            //    .IsFixedLength()
            //    .IsUnicode(false);
        }
    }
}
