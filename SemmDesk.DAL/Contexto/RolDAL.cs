﻿namespace SemmDesk.DAL.Contexto
{
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;

    public class RolDAL : ICRUD<Role>
    {
        private readonly Modelo db = new Modelo();

        public List<Role> GetAll()
        {
            var obj = db.Roles.ToList();
            return obj;
        }

        public Role GetById(int id)
        {
            Role obj = db.Roles.Find(id);
            return obj;
        }

        public void Insert(Role obj)
        {
            db.Roles.Add(obj);
            db.SaveChanges();
        }

        public void Update(Role obj)
        {
            db.Set<Role>().AddOrUpdate(obj);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            Role obj = db.Roles.Find(id);
            db.Roles.Remove(obj);
            db.SaveChanges();
        }
    }
}
