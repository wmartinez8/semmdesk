﻿namespace SemmDesk.DAL.Contexto
{
    using SemmDesk.DAL.Interfaces;
    using SemmDesk.Entities.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;

    public class CategoriaDAL : ICRUD<Categoria>
    {
        private readonly Modelo db = new Modelo();

        public List<Categoria> GetAll()
        {
            var obj = db.Categorias.ToList();
            return obj;
        }

        public Categoria GetById(int id)
        {
            Categoria obj =  db.Categorias.Find(id);
            return obj;
        }

        public void Insert(Categoria obj)
        {
            db.Categorias.Add(obj);
            db.SaveChanges();
        }

        public void Update(Categoria obj)
        {
            db.Set<Categoria>().AddOrUpdate(obj);
            db.SaveChanges();
        }
        public void Delete(int id)
        {
            Categoria obj = db.Categorias.Find(id);
            db.Categorias.Remove(obj);
            db.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
