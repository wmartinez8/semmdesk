﻿
var oTable;

$(function () {

    $('form').attr('autocomplete', 'off');    

    $("#loaderbody").addClass('hide');

    $(document).bind('ajaxStart', function () {
        $("#loaderbody").removeClass('hide');
    }).bind('ajaxStop', function () {
        $("#loaderbody").addClass('hide');
    });
});

showInPopup = (url, title) => {
    $.ajax({
        type: 'GET',
        url: url,
        success: function (res) {
            $('#form-modal .modal-body').html(res);
            $('#form-modal .modal-title').html(title);
            $('#form-modal').modal('show');

            var tm = document.getElementsByClassName("tel-mask");
            if (tm.length > 0) {
                $('.tel-mask').inputmask('(999) 999-9999', { 'placeholder': '(___) ___-____' });
            }
            var ta = document.getElementsByClassName("autocomplete");
            if (ta.length > 0) {
                tieneAutoComplete();
            }
        }
    })
}

showListInPopup = (url, title) => {
    $.ajax({
        type: 'GET',
        url: url,
        success: function (res) {
            $('#modalList .modal-body').html(res);
            $('#modalList .modal-title').html(title);
            $('#modalList').modal('show');
        }
    })
}

jQueryAjaxPost = form => {
    try {
        $.ajax({
            type: 'POST',
            url: form.action,
            data: new FormData(form),
            contentType: false,
            processData: false,
            success: function (res) {
                if (res.isValid) {
                    $('#view-all').html(res.html)
                    $('#form-modal .modal-body').html('');
                    $('#form-modal .modal-title').html('');
                    $('#form-modal').modal('hide');
                    bindDataTable();
                }
                else {
                    $('#form-modal .modal-body').html(res.html);
                    var tm = document.getElementsByClassName("tel-mask");
                    if (tm.length > 0) {
                        $('.tel-mask').inputmask('(999) 999-9999', { 'placeholder': '(___) ___-____' });
                    }
                }
            },
            error: function (err) {
                console.log(err)
            }
        })
        //to prevent default form submit event
        return false;
    } catch (ex) {
        console.log(ex)
    }
}

$(document).ready(function () {
    bindDataTable();
    
 
});

function bindDataTable() {

    var dmin = document.getElementById("tbDataMin");
    var dmax = document.getElementById("tbDataMax");
   
    if (dmin || dmax) {
        var scriptTag =
            "<link rel='stylesheet' href='/HelpDesk/Content/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css'>" +
            "<link rel = 'stylesheet' href = '/HelpDesk/Content/plugins/datatables-responsive/css/responsive.bootstrap4.min.css'>" +
            "<link rel='stylesheet' href='/HelpDesk/Content/plugins/datatables-buttons/css/buttons.bootstrap4.min.css'>" +
            "<script src='/HelpDesk/Content/plugins/datatables/jquery.dataTables.min.js'></script>" +
            "<script src='/HelpDesk/Content/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js'></script>" +
            "<script src='/HelpDesk/Content/plugins/datatables-responsive/js/dataTables.responsive.min.js'></script>" +
            "<script src='/HelpDesk/Content/plugins/datatables-responsive/js/responsive.bootstrap4.min.js'></script>" +
            "<script src='/HelpDesk/Content/plugins/datatables-buttons/js/dataTables.buttons.min.js'></script>" +
            "<script src='/HelpDesk/Content/plugins/datatables-buttons/js/buttons.bootstrap4.min.js'></script>" +
            "<script src='/HelpDesk/Content/plugins/jszip/jszip.min.js'></script>" +
            "<script src='/HelpDesk/Content/plugins/pdfmake/pdfmake.min.js'></script>" +
            "<script src='/HelpDesk/Content/plugins/pdfmake/vfs_fonts.js'></script>" +
            "<script src='/HelpDesk/Content/plugins/datatables-buttons/js/buttons.html5.min.js'></script>" +
            "<script src='/HelpDesk/Content/plugins/datatables-buttons/js/buttons.print.min.js'></script>" +
            "<script src='/HelpDesk/Content/plugins/datatables-buttons/js/buttons.colVis.min.js'></script>";

        $("head").append(scriptTag);

        jQuery.fn.DataTable.ext.type.search.string = function (data) {
            return !data ?
                '' :
                typeof data === 'string' ?
                    data
                        .replace(/έ/g, 'ε')
                        .replace(/ύ/g, 'υ')
                        .replace(/ό/g, 'ο')
                        .replace(/ώ/g, 'ω')
                        .replace(/ά/g, 'α')
                        .replace(/ί/g, 'ι')
                        .replace(/ή/g, 'η')
                        .replace(/\n/g, ' ')
                        .replace(/[áÁ]/g, 'a')
                        .replace(/[éÉ]/g, 'e')
                        .replace(/[íÍ]/g, 'i')
                        .replace(/[óÓ]/g, 'o')
                        .replace(/[úÚ]/g, 'u')
                        .replace(/ê/g, 'e')
                        .replace(/î/g, 'i')
                        .replace(/ô/g, 'o')
                        .replace(/è/g, 'e')
                        .replace(/ï/g, 'i')
                        .replace(/ü/g, 'u')
                        .replace(/ã/g, 'a')
                        .replace(/õ/g, 'o')
                        .replace(/ç/g, 'c')
                        .replace(/ì/g, 'i') :
                    data;
        };

        //jQuery.fn.DataTable.render.ellipsis = function () {
        //    return function (data, type, row) {
        //        return type === 'display' && data.length > 10 ?
        //            data.substr(0, 70) + '…' :
        //            data;
        //    }
        //};
    }

    if (dmax) {

        oTable = $('#tbDataMax').DataTable({
            dom: 'Brtip',
            "autoWidth": false,
            "pagingType": "full_numbers",
            "responsive": true,
            "lengthChange": true,
            "bFilter": true,
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/Bancos/GetBancos",
                "type": "POST",
                "datatype": 'json',
                //dataSrc: function (d) {
                //    columnNames = Object.keys(d.data[0]);
                //    for (var i in columnNames) {
                //        columns.push({
                //            data: columnNames[i],
                //            title: capitalizeFirstLetter(columnNames[i])
                //        });
                //    }
                //    return d.data;
                //}
            },
            columnDefs: [{ "orderable": false, "searchable": false, "targets": [3, 4] }],
            "columns": [
                { "data": "nombre", "name": "Nombre" },
                { "data": "telefono", "name": "Telefono" },
                {
                    "data": "web",
                    "name": "Web",
                    "render": function (web, type, full, meta) {
                        return '<a href="' + web + '" target="_blank">' + web + '</a>'
                    }
                },
                {
                    "data": "activo",
                    "name": "Activo",
                    "className": 'text-center',
                    "render": function (activo, type, full, meta) {
                        return activo ? '<i class="fas fa-check-circle text-success"></i>' : '<i class="fas fa-ban text-danger"></i>'
                    }
                },
                {
                    "data": "id",
                    "name": "Id",
                    "className": 'text-center',
                    "render": function (id, type, full, meta) {
                        return '<a href="/Bancos/Editar/' + id + '"><i class="fas fa-pen-alt text-primary"></i></a>'
                    }
                },
              
            ],
            buttons: [
                {
                    extend: 'collection',
                    text: '<i class="fas fa-file-export"></i> Exportar',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: [0, 1, 2]
                            }
                        }
                    ]
                }
            ],
            "language": {
                "url": "/HelpDesk/Content/plugins/datatables-lang/Spanish.json"
            },
            initComplete: (settings, json) => {
                $('.dataTables_paginate').appendTo('#pagination');
                $('.dataTables_info').appendTo('#info');
                $('.dt-buttons').appendTo('#dv_export');
                $('.buttons-collection').addClass('btn-sm');
                $('.buttons-collection').removeClass('btn-secundary');
                $('.buttons-collection').addClass('btn-default');
            },
        });

        $('.global_filter').on('keyup', function () {
            $('#tbDataMax').DataTable()
                .search(
                    jQuery.fn.DataTable.ext.type.search.string($('.global_filter').val())
                )
                .draw();
        });

    }

    if (dmin) {       
        oTable = $('#tbDataMin').DataTable({
            dom: 'Brtip',
            "order": [[0, "desc"]],
            "autoWidth": true,
            "pagingType": "full_numbers",
            "responsive": true,
            "lengthChange": false,
            "bFilter": true,
            "pageLength": 25,
            "buttons": [/*"copy", "csv",*/ "excel", "pdf", "print", "colvis"],

            //async: true,
            //columnDefs: [{
            //    "orderable": false,
            //    "searchable": false,
            //    "targets": [1],
               
            //}],

            //buttons: [
            //    {
            //        extend: 'collection',
            //        text: '<i class="fas fa-file-export"></i> Exportar',
            //        buttons: [
            //            {
            //                extend: 'excelHtml5',
            //                exportOptions: {
            //                    columns: ':visible'
            //                }
            //            },
            //            {
            //                extend: 'pdfHtml5',
            //                exportOptions: {
            //                    columns: [0, 1, 2]
            //                }
            //            }
            //        ]
            //    }
            //],
            "language": {
                "url": "/HelpDesk/Content/plugins/datatables-lang/Spanish.json"
            },
            initComplete: (settings, json) => {
                $('.dataTables_paginate').appendTo('#pagination');
                $('.dataTables_info').appendTo('#info');
                $('.dt-buttons').appendTo('#dv_export');
                $('.buttons-collection').addClass('btn-sm');
                $('.buttons-collection').removeClass('btn-secundary');
                $('.buttons-collection').addClass('btn-default');
            }


        });

        $('.global_filter').on('keyup', function () {
            $('#tbDataMin').DataTable()
                .search(
                    jQuery.fn.DataTable.ext.type.search.string($('.global_filter').val())
                )
                .draw();
        });
    }

    
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

//function tieneAutoComplete() {

//    $(".autocomplete").autocomplete({
//        source: function (request, response) {
//            $.ajax({
//                url: '/Administracion/Departamentos/OnPostAutoComplete',
//                beforeSend: function (xhr) {
//                    xhr.setRequestHeader("XSRF-TOKEN",
//                        $('input:hidden[name="__RequestVerificationToken"]').val());
//                },
//                type: 'POST',
//                cache: false,
//                data: { "prefix": request.term },
//                dataType: 'json',
//                success: function (data) {
//                    response($.map(data, function (item) {
//                        return {
//                            label: item.label,
//                            value: item.value
//                        }
//                    }))
//                }
//            });
//        },
//        minLength: 3,
//        select: function (event, ui) {
//            $(".autocomplete").val(ui.item.label);
//            $("#EncargadoId").val(ui.item.value);
//            return false;
//        }
//    });
//}