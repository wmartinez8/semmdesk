﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SemmDesk.Web.Models
{
    public class UsuarioViewModel
    {
        public int UsuarioId { get; set; }
        public string Identificador { get; set; }
        public string Nombres { get; set; }
        public int CodigoEmpleado { get; set; }
        public int Departamento { get; set; }
        public int? Rol { get; set; }
        public string Estatus { get; set; }
    }
}