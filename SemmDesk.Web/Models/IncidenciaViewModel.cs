﻿namespace SemmDesk.Web.Models
{
    using SemmDesk.Entities.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;

    public class IncidenciaViewModel
    {
        [Display(Name = "ID #")]
        public int IncidenciaId { get; set; }

        [Display(Name = "DESCRIPCIÓN")]
        public string Descripcion { get; set; }

        [Display(Name = "CATEGORÍA")]
        public int CategoriaId { get; set; }

        [Display(Name = "CATEGORÍA")]
        public string Categoria { get; set; }

        [Display(Name = "SUBCATEGORÍA")]
        public int SubCategoriaId { get; set; }

        [Display(Name = "SUBCATEGORÍA")]
        public string SubCategoria { get; set; }

        [Display(Name = "PRIORIDAD")]
        public int PrioridadId { get; set; }

        [Display(Name = "PRIORIDAD")]
        public string Prioridad { get; set; }

        [Display(Name = "ESTADO")]
        public int EstadoIncidenciaId { get; set; }

        [Display(Name = "ESTADO")]
        public string Estado { get; set; }

        [Display(Name = "EmpleadoID")]
        public int EmpleadoId { get; set; }

        [Display(Name = "Empleado")]
        public string EmpleadoNombre { get; set; }

        [Display(Name = "DEPARTAMENTO ID")]
        public int DepartamentoID { get; set; }

        [Display(Name = "DEPARTAMENTO")]
        public string Departamento { get; set; }

        [Display(Name = "TÉCNICO")]
        public int TecnicoId { get; set; }

        [Display(Name = "TÉCNICO")]
        public string Tecnico { get; set; }

        [Display(Name = "NOTA DE CIERRE")]
        public string Comentario { get; set; }

        //[Display(Name = "TÉCNICO REASIGNADO")]
        //public int? TecnicoReasignado { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:MM}")]
        [Display(Name = "FECHA")]
        [DataType(DataType.Date)]
        public DateTime FechaIncidencia { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:MM}")]
        [Display(Name = "CIERRE")]
        [DataType(DataType.Date)]
        public DateTime? FechaCierre { get; set; }

        public int UsuarioCreacion { get; set; }

        public int AreaId { get; set; }
        
        public string Area { get; set; }
        
        public Tecnico Tecnicos { get; set; }
        public Empleado Empleados { get; set; }

    }
}