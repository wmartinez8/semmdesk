﻿namespace SemmDesk.Web.Models
{
    using System;

    public class EmpleadoViewModel
    {
        public int EmpleadoId { get; set; }
        public string Nombres { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string Identificador { get { return Nombres.Substring(0,1) + Apellido1.Trim(); } set { } }
        public int CodigoEmpleado { get; set; }
        public string Departamento { get; set; }
        public string Foto { get; set; }
        public int RolId { get; set; }
        public string Estatus { get; set; }
        public string Clave { get; set; }
        public string Sexo { get; set; }
        public int DepartamentoId { get; set; }
        public string Telefono { get; set; }
        public string Cargo { get; set; }
        public DateTime FechaNacimiento { get; set; }
    }
}