﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SemmDesk.Web.Models
{
    public enum EnumTecnicoRol
    {
        Administrador = 1,
        TecnicoTIC = 2,
        TecnicoServicios = 3,
        Usuario = 4
    }
}