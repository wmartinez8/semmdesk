﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SemmDesk.Web.Models
{
    public class TecnicoViewModel
    {
        public int TecnicoId { get; set; }
        public int CodigoEmpleado { get; set; }
        public string Nombres { get; set; }
        public string Identificador { get; set; }
        public int RolId { get; set; }
        public string Clave { get; set; }
    }
}