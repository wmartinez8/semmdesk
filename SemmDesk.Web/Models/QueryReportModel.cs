﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SemmDesk.Web.Models
{
    public class QueryReportModel
    {
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFinal { get; set; }
        public int? CategoriaId { get; set; }
        public int? SubCategoriaId { get; set; }
        public int? TecnicoId { get; set; }
        public int? EstadoId { get; set; }

    }
}