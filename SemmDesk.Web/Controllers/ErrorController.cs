﻿namespace SemmDesk.Web.Controllers
{
    using System.Web.Mvc;

    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UnAuthorized()
        {
            return View();
        }
    }
}