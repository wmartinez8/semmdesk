﻿namespace SemmDesk.Web.Controllers
{
    using SemmDesk.BLL;
    using SemmDesk.DAL.Repositorios;
    using SemmDesk.Entities.Models;
    using SemmDesk.Web.Models;
    using System.Collections;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Mvc;


    public class EmpleadosController : Controller
    {
        
        private readonly Empleado _empleado;
        private readonly EmpleadoBLL _empleadoBLL;
        private readonly PasswordServices _passwordServices;

        public EmpleadosController()
        {
            this._empleado = new Empleado();
            this._empleadoBLL = new EmpleadoBLL();
            this._passwordServices = new PasswordServices();
        }
        // GET: Empleados
        public ActionResult Index()
        {
            var lsEmpleados = _empleadoBLL.GetAll();
            //TODO: convertir a viewModel para pasar a la vista
            List<EmpleadoViewModel> lsEmpleadoViewModel = new List<EmpleadoViewModel>();
            foreach (var item in lsEmpleados)
            {
                EmpleadoViewModel empleadoViewModel =  new EmpleadoViewModel { 
                    EmpleadoId = item.EmpleadoId,
                    Nombres = item.Nombres,
                    Apellido1 = item.Apellido1,
                    Apellido2 = item.Apellido2,
                    CodigoEmpleado = item.Codigo,
                    Departamento = item.Departamento,
                    Foto = item.Foto,
                    RolId = item.RolId,
                    Estatus = item.Estatus
                };
                lsEmpleadoViewModel.Add(empleadoViewModel);
            }
            return View(lsEmpleadoViewModel);
        }

        
        public ActionResult ChangeUserPassowrd()
        {
            var empleado = (Empleado)Session["Usuario"];
            //TODO: CAMBIAR A AUTOMAPPER
            //TODO: CAMBIAR ESTA VISA ALGO ASI TIPO CARD CON LA FOTO DEL EMPLEDO Y CONFIRMACION DE CONTRASENA
            EmpleadoViewModel empleadoViewModel = new EmpleadoViewModel { 
                EmpleadoId = empleado.EmpleadoId,
                Nombres = empleado.Nombres,
                Apellido1 = empleado.Apellido1,
                Apellido2 = empleado.Apellido2,
                CodigoEmpleado = empleado.Codigo,
                Departamento = empleado.Departamento,
                Foto = empleado.Foto,
                RolId = empleado.RolId,
                Estatus = empleado.Estatus,
                Clave = empleado.Clave,
                Sexo = empleado.Sexo,
                DepartamentoId = empleado.DepartamentoId,
                Telefono = empleado.Telefono,
                Cargo = empleado.Cargo,
                FechaNacimiento = empleado.FechaNacimiento
                
            };
            return View(empleadoViewModel);
        }

        [HttpPost]
        public ActionResult ChangeUserPassowrd(EmpleadoViewModel empleadoView, string nuevaClave)
        {
            nuevaClave = _passwordServices.Hash(nuevaClave);
            var empleado = new Empleado() {
                EmpleadoId = empleadoView.EmpleadoId,
                Nombres = empleadoView.Nombres,
                Apellido1 = empleadoView.Apellido1,
                Apellido2 = empleadoView.Apellido2,
                Codigo = empleadoView.CodigoEmpleado,
                Departamento = empleadoView.Departamento,
                Foto = empleadoView.Foto,
                RolId = empleadoView.RolId,
                Estatus = empleadoView.Estatus,
                Clave = empleadoView.Clave,
                Sexo = empleadoView.Sexo,
                DepartamentoId = empleadoView.DepartamentoId,
                Telefono = empleadoView.Telefono,
                Cargo = empleadoView.Cargo,
                FechaNacimiento = empleadoView.FechaNacimiento
            }; 
            var empledoDb = _empleadoBLL.ChangePassword(empleado, nuevaClave);
            if (empledoDb ==false)
            {
                ViewData["Error"] = "ERROR CAMBIANDO CONTRASEÑA";
                return Redirect(Url.Action("ChangeUserPassowrd"));
            }

            return Redirect(Url.Action("Index","Incidencias"));

        }
    }
}