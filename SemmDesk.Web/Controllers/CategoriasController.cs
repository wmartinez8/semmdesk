﻿using SemmDesk.BLL;
using SemmDesk.Entities.Models;
using SemmDesk.Web.Repositorios;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SemmDesk.Web.Controllers
{

    public class CategoriasController : Controller
    {
        private readonly CategoriaBLL _categoriaBLL;
        public CategoriasController()
        {
            this._categoriaBLL = new CategoriaBLL();
        }

        [FiltroAuthorize(idOperacion: 1)]
        public ActionResult Index()
        {
            var categorias = _categoriaBLL.GetAll();
            return View(categorias);
        }

        [FiltroAuthorize(idOperacion: 1)]
        public ActionResult Crear()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [FiltroAuthorize(idOperacion: 1)]
        public ActionResult Crear(Categoria categoria)
        {
            if (ModelState.IsValid)
            {
                //No tengo los campos de auditorias en la base de datos. Debo ver eso.
                _categoriaBLL.Insert(categoria);
                return RedirectToAction("Index");
            }
            return View(categoria);
        }

        [FiltroAuthorize(idOperacion: 1)]
        public ActionResult Editar(int id)
        {
            Categoria categoriaDB = _categoriaBLL.GetById(id);
            if (id != categoriaDB.CategoriaId)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (categoriaDB == null)
            {
                return HttpNotFound();
            }

            return View(categoriaDB);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [FiltroAuthorize(idOperacion: 1)]
        public ActionResult Editar(Categoria categoria)
        {
            if (ModelState.IsValid)
            {
                _categoriaBLL.Update(categoria);
                return RedirectToAction("index");
            }
            return View(categoria);
        }


    }
}