﻿using SemmDesk.BLL;
using SemmDesk.Entities.Models;
using SemmDesk.Web.Repositorios;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SemmDesk.Web.Controllers
{
    public class SubCategoriasController : Controller
    {
        private readonly SubCategoriaBLL _subCatgoriaBLL;

        private readonly CategoriaBLL _categoriaBLL;
        private readonly IEnumerable<Categoria> _lsCategorias;

        public SubCategoriasController()
        {
            this._subCatgoriaBLL = new SubCategoriaBLL();
            this._categoriaBLL = new CategoriaBLL();
            this._lsCategorias = (IEnumerable<Categoria>)_categoriaBLL.GetAll();
        }

        [FiltroAuthorize(idOperacion: 1)]
        public ActionResult Index()
        {
            IEnumerable<SubCategoria> lsCategorias = _subCatgoriaBLL.GetAll();
            return View(lsCategorias);
        }

        [FiltroAuthorize(idOperacion: 1)]
        public ActionResult Crear()
        {
            SubCategoria subCategoria = new SubCategoria();
            ViewBag.lsCategorias = new SelectList((System.Collections.IEnumerable)_lsCategorias, "CategoriaId", "Descripcion", subCategoria.SubCategoriaId);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [FiltroAuthorize(idOperacion: 1)]
        public ActionResult Crear(SubCategoria subCategoria)
        {
            if (ModelState.IsValid)
            {
                _subCatgoriaBLL.Insert(subCategoria);
                return RedirectToAction("Index");
            }
            return View(subCategoria);
        }

        [FiltroAuthorize(idOperacion: 1)]
        public ActionResult Editar(int id)
        {
            SubCategoria subCategoriaDB = _subCatgoriaBLL.GetById(id);
            if (subCategoriaDB.SubCategoriaId != id)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (subCategoriaDB == null)
            {
                return HttpNotFound();
            }

            ViewBag.lsCategorias = new SelectList(_lsCategorias, "CategoriaId", "Descripcion", subCategoriaDB.SubCategoriaId);
            return View(subCategoriaDB);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [FiltroAuthorize(idOperacion: 1)]
        public ActionResult Editar(SubCategoria subCategoria)
        {
            if (ModelState.IsValid)
            {
                _subCatgoriaBLL.Update(subCategoria);
                return RedirectToAction("index");
            }
            return View(subCategoria);
        }
    }
}