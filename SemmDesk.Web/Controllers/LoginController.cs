﻿namespace SemmDesk.Web.Controllers
{
    using SemmDesk.BLL;
    using SemmDesk.DAL.Repositorios;
    using SemmDesk.Entities.Models;
    using SemmDesk.Web.Models;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    public class LoginController : Controller
    {
        //private readonly Empleado _empleado;
        private readonly EmpleadoBLL _empleadoBLL;
        private readonly TecnicoBLL _tecnicoBLL;
        private readonly PasswordServices _passwordServices;

        public LoginController()
        {
            //this._empleado = new Empleado();
            this._empleadoBLL = new EmpleadoBLL();
            this._tecnicoBLL = new TecnicoBLL();
            this._passwordServices = new PasswordServices();
        }

        // GET: Login
        public ActionResult Login()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(int CodigoEmpleado)
        {
            if (ModelState.IsValid)
            {
                var empleadoDB = _empleadoBLL.Login(CodigoEmpleado);
                if (empleadoDB != null)
                {
                    Session["Usuario"] = empleadoDB;

                    //TODO Entiendo que a la Session[Usuario] puedo obtener c/u de las propiedades de obj.
                    // No se como obtejer Session[Usuario].Nombres por ejemplo. Por eso tuve que crear una instancia de EmpleadoViewModel
                    EmpleadoViewModel emp = new EmpleadoViewModel
                    {
                        EmpleadoId = empleadoDB.EmpleadoId,
                        CodigoEmpleado = empleadoDB.Codigo,
                        Nombres = empleadoDB.Nombres.Trim(),
                        Apellido1 = empleadoDB.Apellido1,
                        Apellido2 = empleadoDB.Apellido2,
                        Identificador = empleadoDB.Nombres,
                        RolId = empleadoDB.RolId,
                        Departamento = empleadoDB.Departamento,
                        Foto = empleadoDB.Foto
                    };
                    Session["CodigoEmpleado"] = emp.CodigoEmpleado;
                    Session["NombreUsuario"] = emp.Identificador;
                    Session["Foto"] = emp.Foto;


                    Tecnico tecnico = _tecnicoBLL.GetById(empleadoDB.Codigo);
                    if (tecnico != null)
                    {
                        //ROL 1 = TECNICO
                        if (tecnico.RolId == (int)EnumTecnicoRol.Administrador
                                        || empleadoDB.RolId == (int)EnumTecnicoRol.TecnicoTIC
                                        || empleadoDB.RolId == (int)EnumTecnicoRol.TecnicoServicios)
                        {
                            //return RedirectToAction("Index", "Incidencias");
                            return RedirectToAction("ConfirmarTecnico", "Login", tecnico);

                        };
                    }

                    //ROL 2 = USUARIO
                    if (empleadoDB.RolId == (int)EnumTecnicoRol.Usuario)
                    {
                        return RedirectToAction("Index", "Incidencias");
                    }
                }
                ViewBag.UsuarioNoEncontrado = "ESTE USUARIO NO ES VALIDO!";
            }
            return View();
        }

        public ActionResult ConfirmarTecnico(Tecnico tecnico)
        {
            return View(tecnico);
        }

        [HttpPost]
        public async Task<ActionResult> ConfirmarTecnico(Tecnico tecnico, string clave)
        {
            var tecnicoDB = _tecnicoBLL.GetById(tecnico.CodigoEmpleado);
            if (tecnicoDB != null)
            {
                var validation = await IsValidUser(tecnicoDB, clave);
                if (validation.Item1)
                {
                    return RedirectToAction("Index", "Incidencias");
                }
            }
            ViewData["Password"] = "CONTRASEÑA NO VALIDA!";
            return View(tecnico);

        }

        private async Task<(bool, Tecnico)> IsValidUser(Tecnico login, string clave)
        {
            //Tecnico user = _tecnicoBLL.GetById(login.TecnicoId);
            var isValid = _passwordServices.Check(login.Clave, clave);
            return (isValid, login);

        }

        public ActionResult LogOut()
        {
            Session.Abandon();
            return RedirectToAction("Login", "Login");
        }
    }
}