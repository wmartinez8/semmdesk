﻿using SemmDesk.BLL;
using SemmDesk.DAL.Contexto;
using SemmDesk.Entities.Models;
using SemmDesk.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SemmDesk.Web.Controllers
{
    public class IncidenciasController : Controller
    {
        private readonly IncidenciaBLL _incidenciasBLL;
        private readonly Modelo db;
        private readonly EmpleadoBLL _empleadosBLL;

        public IncidenciasController()
        {
            this._incidenciasBLL = new IncidenciaBLL();
            this._empleadosBLL = new EmpleadoBLL();
            this.db = new Modelo();

        }

        public ActionResult Index(DateTime? fechaInicio, DateTime? fechaFinal)
        {

            var empleado = (Empleado)Session["Usuario"];
            ViewData["IsTecnico"] = empleado.RolId;
            List<IncidenciaViewModel> list = new List<IncidenciaViewModel>();

            if (empleado.RolId == (int)EnumTecnicoRol.Usuario)
            {
                //TODO: Ahora el id de empleado es difernte para las incidencias, antes era el codigo del empleado.
                // Sigo usando el codigo para agregar en EmpleadoId de la incidencia o mando el Id de la tabla empleado
                // Como cambiarla a las incidencias anteriores? 

                var lsIncidencias = (from i in db.Incidencias
                                     join p in db.Prioridades
                                     on i.PrioridadID equals p.PrioridadId
                                     join e in db.EstadoIncidencias on
                                     i.EstadoIncidenciaID equals e.EstadoIncidenciaId
                                     join emp in db.Empleados on
                                     i.EmpleadoId equals emp.Codigo
                                     join t in db.Tecnicos on
                                     i.TecnicoID equals t.TecnicoId
                                     where i.TecnicoID == t.TecnicoId && i.UsuarioCreacion == empleado.Codigo
                                     select new
                                     {
                                         Incidencia = i.IncidenciaId,
                                         Fecha = i.FechaCreacion,
                                         Descripcion = i.Descripcion,
                                         Estado = e.Descripcion,
                                         Comentario = i.ComentarioCierre
                                     }
                               ).ToList();


                foreach (var item in lsIncidencias)
                {
                    IncidenciaViewModel newIncidencia = new IncidenciaViewModel
                    {
                        IncidenciaId = item.Incidencia,
                        FechaIncidencia = item.Fecha,
                        Descripcion = item.Descripcion,
                        Estado = item.Estado,
                        Comentario = item.Comentario,
                    };
                    list.Add(newIncidencia);
                };


                return View("IndexUsuario", list.OrderByDescending(x => x.IncidenciaId));
            }
            else
            {
                var lsIncidencias = (from i in db.Incidencias
                                     join p in db.Prioridades
                                     on i.PrioridadID equals p.PrioridadId
                                     join e in db.EstadoIncidencias 
                                     on i.EstadoIncidenciaID equals e.EstadoIncidenciaId
                                     join emp in db.Empleados on
                                     i.EmpleadoId equals emp.Codigo
                                     join t in db.Tecnicos on
                                     i.TecnicoID equals t.TecnicoId
                                     join s in db.SubCategorias on
                                     i.SubCategoriaID equals s.SubCategoriaId
                                     join a in db.Areas on
                                     i.AreaId equals a.AreaId
                                     where i.EmpleadoId == emp.Codigo || i.EmpleadoId == emp.EmpleadoId//i.TecnicoID == t.TecnicoId
                                     select new
                                     {
                                         Incidencia = i.IncidenciaId,
                                         Fecha = i.FechaCreacion,
                                         Descripcion = i.Descripcion,
                                         Prioridad = p.Descripcion,
                                         Categoria = i.Categoria.Descripcion,
                                         Subcategoria = i.SubCategoria.Descripcion,
                                         Estado = e.Descripcion,
                                         Empleado = emp.Nombres,
                                         Tecnico = t.Identificador,
                                         Comentario = i.ComentarioCierre,
                                         Area = a.NombreArea
                                     }
                               ).ToList();

                foreach (var item in lsIncidencias)
                {
                    IncidenciaViewModel newIncidencia = new IncidenciaViewModel
                    {                       
                        IncidenciaId = item.Incidencia,
                        FechaIncidencia = item.Fecha,
                        Descripcion = item.Descripcion,
                        Categoria = item.Categoria,
                        SubCategoria = item.Subcategoria,
                        Prioridad = item.Prioridad,
                        Estado = item.Estado,
                        EmpleadoNombre = item.Empleado,
                        Tecnico = item.Tecnico,
                        Comentario = item.Comentario,
                        Area = item.Area

                    };
                    list.Add(newIncidencia);
                };

                ViewData["Total"] = lsIncidencias.Count();
                ViewData["Abiertas"] = lsIncidencias.Count(x => x.Estado == "ABIERTA");
                ViewData["Cerradas"] = lsIncidencias.Count(x => x.Estado == "CERRADA");
                ViewData["Retrasadas"] = lsIncidencias.Count();

                var fn = Convert.ToDateTime(fechaFinal).Date;
                if (fechaInicio != null && fechaFinal != null)
                {
                    var inicio = fechaInicio.Value.AddDays(-1).ToString(); //fechaInicio.Value.ToString("yyyy/MM/dd");
                    var final = fechaFinal.Value.AddDays(1).ToString(); //.Value.ToString("yyyy/MM/dd");

                    //DateTime FechaFinal = Convert.ToDateTime(fechaFinal);
                    //FechaFinal = FechaFinal.AddDays(1);

                    var lista = list.OrderBy(x => x.IncidenciaId).Where(x => x.FechaIncidencia >= DateTime.Parse(inicio) && x.FechaIncidencia <= DateTime.Parse(final));
                    return View(lista);
                }
                fechaInicio = DateTime.Now;
                fechaFinal = DateTime.Now;

                return View(list.OrderByDescending(x => x.IncidenciaId));

            }

        }

        public ActionResult Crear()
        {
            #region Llenando ViewBags para mandar a la vista
            var lsDepartamentos = db.vDepartamentos.ToList().OrderBy(d => d.descrip);
            var lsCategorias = db.Categorias.ToList().OrderBy(c => c.Descripcion);
            var lsSubCategorias = db.SubCategorias.ToList().OrderBy(s => s.Descripcion);
            var lsPrioridades = db.Prioridades.ToList().OrderByDescending(x => x.Descripcion);
            var lsEmpleados = _empleadosBLL.GetAll(); //db.Empleados.ToList().Where(x => x.Estatus != "C" || x.Estatus != "E").OrderBy(u => u.Nombres);
            var lsTecnicos = db.Tecnicos.ToList().OrderBy(t => t.Nombres);
            var lsEstadoIncidencia = db.EstadoIncidencias.ToList().OrderBy(e => e.Descripcion);
            var lsAreas = db.Areas.ToList().OrderByDescending(e => e.NombreArea);

            ViewData["lsDepartamentos"] = new SelectList(lsDepartamentos, "departam", "descrip");
            ViewData["lsCategorias"] = new SelectList(lsCategorias, "CategoriaId", "Descripcion");
            ViewData["lsSubCategorias"] = new SelectList(lsSubCategorias, "SubCategoriaId", "Descripcion");
            ViewData["lsPrioridades"] = new SelectList(lsPrioridades, "PrioridadId", "Descripcion");
            ViewData["lsEmpleados"] = new SelectList(lsEmpleados, "Codigo", "Nombres");
            ViewData["lsTecnicos"] = new SelectList(lsTecnicos, "TecnicoId", "Nombres");
            ViewData["lsEstadoIncidencia"] = new SelectList(lsEstadoIncidencia, "EstadoIncidenciaId", "Descripcion");
            ViewBag.lsAreas = new SelectList(lsAreas, "AreaId", "NombreArea");

            #endregion

            //TODO Igual aqui con relacion a obtener c/u de las propiedades de Session[Usuario].RoilId
            var empleado = (Empleado)Session["Usuario"];
            Session["IsTecnico"] = empleado.EmpleadoId;

            if ((int)Session["IsTecnico"] == (int)EnumTecnicoRol.Usuario)
            {
                return View("_partialCrearUsuario");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Crear(IncidenciaViewModel vm)
        {
            #region Llenando ViewBags para mandar a la vista
            var lsDepartamentos = db.vDepartamentos.ToList().OrderBy(d => d.descrip);
            var lsCategorias = db.Categorias.ToList().OrderBy(c => c.Descripcion);
            var lsSubCategorias = db.SubCategorias.ToList().OrderBy(s => s.Descripcion);
            var lsPrioridades = db.Prioridades.ToList();
            var lsEmpleados = _empleadosBLL.GetAll();//db.Empleados.Where(x => x.Estatus != "C" || x.Estatus != "E").OrderBy(u => u.Nombres).ToList();
            var lsTecnicos = db.Tecnicos.ToList().OrderBy(t => t.Nombres);
            var lsEstadoIncidencia = db.EstadoIncidencias.ToList().OrderBy(e => e.Descripcion);
            var lsAreas = db.Areas.ToList().OrderBy(e => e.NombreArea);

            ViewData["lsDepartamentos"] = new SelectList(lsDepartamentos, "departam", "descrip");
            ViewData["lsCategorias"] = new SelectList(lsCategorias, "CategoriaId", "Descripcion");
            ViewBag.lsSubCategorias = new SelectList(lsSubCategorias, "SubCategoriaId", "Descripcion");
            ViewBag.lsPrioridades = new SelectList(lsPrioridades, "PrioridadId", "Descripcion");
            ViewBag.lsEmpleados = new SelectList(lsEmpleados, "Codigo", "Nombres");
            ViewBag.lsTecnicos = new SelectList(lsTecnicos, "TecnicoId", "Nombres");
            ViewBag.lsEstadoIncidencia = new SelectList(lsEstadoIncidencia, "EstadoIncidenciaId", "Descripcion");
            ViewBag.lsAreas = new SelectList(lsAreas, "AreaId", "NombreArea");
            #endregion

            Incidencia incidencia = new Incidencia
            {
                EmpleadoId = vm.EmpleadoId,
                CategoriaID = vm.CategoriaId,
                SubCategoriaID = vm.SubCategoriaId,
                Descripcion = vm.Descripcion,
                PrioridadID = vm.PrioridadId,
                EstadoIncidenciaID = vm.EstadoIncidenciaId,
                ComentarioCierre = vm.Comentario,
                TecnicoID = vm.TecnicoId,
                AreaId = vm.AreaId

            };
            if (ModelState.IsValid)
            {
                incidencia.UsuarioCreacion = (int)Session["CodigoEmpleado"];
                _incidenciasBLL.Insert(incidencia);
                return RedirectToAction("index");
            }
            return View(vm);
        }

        public ActionResult Editar(int id)
        {
            Incidencia incidenciaDB = _incidenciasBLL.GetById(id);
            if (id != incidenciaDB.IncidenciaId)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (incidenciaDB == null)
            {
                return HttpNotFound();

            }

            EmpleadoBLL empleadoBLL = new EmpleadoBLL();
            var usuario = empleadoBLL.GetById(incidenciaDB.EmpleadoId);
            
            IncidenciaViewModel vm = new IncidenciaViewModel
            {
                IncidenciaId = incidenciaDB.IncidenciaId,
                EmpleadoNombre = usuario.Nombres,
                CategoriaId = incidenciaDB.CategoriaID,
                SubCategoriaId = incidenciaDB.SubCategoriaID,
               // SubCategoria = incidenciaDB.SubCategoria.Descripcion,
                Descripcion = incidenciaDB.Descripcion,
                PrioridadId = incidenciaDB.PrioridadID.Value,
                EstadoIncidenciaId = incidenciaDB.EstadoIncidenciaID.Value,
                TecnicoId = incidenciaDB.TecnicoID.Value,
                //Tecnico = incidenciaDB.Tecnico.Nombres,
                Comentario = incidenciaDB.ComentarioCierre,
                EmpleadoId = incidenciaDB.EmpleadoId,
                FechaIncidencia = incidenciaDB.FechaCreacion,
                UsuarioCreacion = incidenciaDB.UsuarioCreacion,
                AreaId = incidenciaDB.AreaId
                //Area = incidenciaDB.Area.NombreArea

            };

            #region Llenando ViewBag para mandar a la vista
            var lsDepartamentos = db.vDepartamentos.ToList().OrderBy(d => d.descrip);
            var lsCategorias = db.Categorias.ToList().OrderBy(c => c.Descripcion);
            var lsSubCategorias = db.SubCategorias.ToList().OrderBy(s => s.Descripcion);
            var lsPrioridades = db.Prioridades.ToList().OrderBy(p => p.Descripcion);
            var lsEmpleados = _empleadosBLL.GetAll().ToList(); //db.Empleados.Where(x => x.Estatus != "C" && x.Estatus != "E").ToList();
            var lsTecnicos = db.Tecnicos.ToList().OrderBy(t => t.Nombres);
            var lsEstadoIncidencia = db.EstadoIncidencias.ToList().OrderBy(e => e.Descripcion);
            var lsAreas = db.Areas.ToList().OrderBy(e => e.NombreArea);


            ViewBag.lsDepartamentos = new SelectList(lsDepartamentos, "departam", "descrip");
            ViewBag.lsCategorias = new SelectList(lsCategorias, "CategoriaId", "Descripcion");
            ViewBag.lsSubCategorias = new SelectList(lsSubCategorias, "SubCategoriaId", "Descripcion");
            ViewBag.lsPrioridades = new SelectList(lsPrioridades, "PrioridadId", "Descripcion");
            ViewBag.lsEmpleados = new SelectList(lsEmpleados.OrderBy(x => x.Nombres), "EmpleadoId", "Nombres");
            ViewBag.lsTecnicos = new SelectList(lsTecnicos, "TecnicoId", "Nombres");
            ViewBag.lsEstadoIncidencia = new SelectList(lsEstadoIncidencia, "EstadoIncidenciaId", "Descripcion");
            ViewBag.lsAreas = new SelectList(lsAreas, "AreaId", "NombreArea");

            #endregion
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(IncidenciaViewModel vm)
        {
            Incidencia incidencia = new Incidencia
            {
                IncidenciaId = vm.IncidenciaId,
                CategoriaID = vm.CategoriaId,
                SubCategoriaID = vm.SubCategoriaId,
                Descripcion = vm.Descripcion,
                PrioridadID = vm.PrioridadId,
                EstadoIncidenciaID = vm.EstadoIncidenciaId,
                EmpleadoId = vm.EmpleadoId,
                TecnicoID = vm.TecnicoId,
                ComentarioCierre = vm.Comentario,
                FechaCreacion = vm.FechaIncidencia,
                // TecnicoReAsignadoID = vm.TecnicoReasignado,
                UltimaModificacion = DateTime.Now,
                UsuarioCreacion = vm.UsuarioCreacion,
                AreaId = vm.AreaId

            };

            if (ModelState.IsValid)
            {
                incidencia.UsuarioModificacion = (int)Session["CodigoEmpleado"];
                _incidenciasBLL.Update(incidencia);
                return RedirectToAction("index");
            }
            return View(incidencia);
        }

        public ActionResult Detalles(int id)
        {
            Incidencia incidenciaDB = _incidenciasBLL.GetById(id);
            try
            {
                if (incidenciaDB == null)
                {
                    return HttpNotFound();
                }
                return View(incidenciaDB);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        //Hago este metodo para que desde la vista create and edit hago una llamada AJAX
        // para cargar dichas subcategoria segun el filtro del dropdownlist
        public JsonResult ListaSubCategorias(int id)
        {
            Modelo db = new Modelo();
            db.Configuration.ProxyCreationEnabled = false; //Con esto evito un error que me sale de Referencia Circular
            List<SubCategoria> lsSubCategorias = db.SubCategorias.Where(x => x.CategoriaId == id).ToList();

            return Json(lsSubCategorias.OrderBy(x => x.Descripcion), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListaCategorias(int id)
        {
            Modelo db = new Modelo();
            db.Configuration.ProxyCreationEnabled = false; //Con esto evito un error que me sale de Referencia Circular
            List<Categoria> lsCategorias = db.Categorias.Where(x => x.CategoriaId == id).ToList();

            return Json(lsCategorias.OrderBy(x => x.Descripcion), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListaEmpleados(int id)
        {
            //Modelo db = new Modelo();
            //db.Configuration.ProxyCreationEnabled = false;
            var lsEmpleados = _empleadosBLL.GetByDepartment(id); // db.Empleados.Where(x => x.DepartamentoId == id && x.Estatus != "C" && x.Estatus != "E").ToList();

            return Json(lsEmpleados, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetincIdencias()
        {
            var lst = _incidenciasBLL.GetAll();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

    }
}