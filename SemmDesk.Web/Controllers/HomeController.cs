﻿using SemmDesk.DAL.Contexto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SemmDesk.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly Modelo _db;

        public HomeController()
        {
            this._db = new Modelo();
        }
        // GET: Home
        public ActionResult Index()
        {
            #region Llenando ViewBags para mandar a la vista
            ViewData["Total"] = _db.Incidencias.Count();
            ViewData["Cerradas"] = _db.Incidencias.Where(x => x.EstadoIncidenciaID == 3).Count();
            ViewData["Abiertas"] = _db.Incidencias.Where(x => x.EstadoIncidenciaID == 1).Count();
            // Pensar en llenar las retrasadas comparando la hora con la actual
            //ViewData["Retrasadas"] = _db.Incidencias.Where(x => x.EstadoIncidenciaID == 1).Count();
            
            #endregion

            return View();
        }
    }
}