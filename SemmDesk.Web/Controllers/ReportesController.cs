﻿namespace SemmDesk.Web.Controllers
{
    using SemmDesk.DAL.Contexto;
    using SemmDesk.Web.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.UI.WebControls;

    public class ReportesController : Controller
    {
        private readonly Modelo _db;

        public ReportesController()
        {
            this._db = new Modelo();
        }
        // GET: Reportes
        public ActionResult Index()
        {
            var lsTecnicos = _db.Tecnicos.ToList().OrderBy(t => t.Nombres);
            var lsCategorias = _db.Categorias.ToList().OrderByDescending(x => x.Descripcion);
            var lsSubCategorias = _db.SubCategorias.ToList().OrderByDescending(x => x.Descripcion);
            var lsEstados = _db.EstadoIncidencias.ToList().OrderByDescending(x => x.Descripcion);

            ViewData["lsTecnicos"] = new SelectList(lsTecnicos, "TecnicoId", "Nombres");
            ViewData["lsCategorias"] = new SelectList(lsCategorias, "CategoriaId", "Descripcion");
            ViewData["lsSubCategorias"] = new SelectList(lsSubCategorias, "SubCategoriaId", "Descripcion");
            ViewData["lsEstados"] = new SelectList(lsEstados, "EstadoIncidenciaId", "Descripcion");
            return View();
        }

        public ActionResult GetReport(QueryReportModel query)
        {
            if (query.FechaInicio ==null)
            {
                query.FechaInicio = Convert.ToDateTime("2020-01-01 00:00:00");
            }
            if (query.FechaFinal == null)
            {
                query.FechaFinal = DateTime.Now;
            }

            var FechaInicio = query.FechaInicio;
            var FechaFinal = query.FechaFinal.Value.AddDays(1);
            var CategoriaId = query.CategoriaId;
            var SubCategoriaId = query.SubCategoriaId;
            var TecnicoId = query.TecnicoId;
            var Estado = query.EstadoId;

            var rIncidencias = (from i in _db.Incidencias
                                join emp in _db.Empleados on i.EmpleadoId equals emp.Codigo
                                join d in _db.vDepartamentos on emp.DepartamentoId equals d.departam
                                //join tec in _db.Tecnicos on emp.Codigo equals tec.CodigoEmpleado
                                    where (i.FechaCreacion >= FechaInicio && i.FechaCreacion <= FechaFinal || FechaInicio == null || FechaFinal == null)
                                       && (i.TecnicoID == TecnicoId || TecnicoId == null)
                                       && (i.CategoriaID == CategoriaId || CategoriaId == null)
                                       && (i.SubCategoriaID == SubCategoriaId || SubCategoriaId == null)
                                       &&(i.EstadoIncidenciaID == Estado || Estado == null)
                                           
                                           select new {i.IncidenciaId, i.FechaCreacion, i.Descripcion, i.Categoria,
                                                        i.SubCategoria, i.Prioridade, i.EstadoIncidencia, i.Tecnico.Identificador, 
                                                        i.ComentarioCierre, i.FechaCierre, emp.Departamento, emp.Nombres, d.descrip
                                                      }).ToList().OrderByDescending(x => x.IncidenciaId);

            List<IncidenciaViewModel> lsIncidenciaViewModel = new List<IncidenciaViewModel>();
            foreach (var item in rIncidencias)
            {
                var incidencia = new IncidenciaViewModel()
                {
                    IncidenciaId = item.IncidenciaId,
                    FechaIncidencia = item.FechaCreacion,
                    Descripcion = item.Descripcion,
                    Departamento = item.descrip,
                    EmpleadoNombre = item.Nombres,
                    Categoria = item.Categoria.Descripcion,
                    SubCategoria = item.SubCategoria.Descripcion,
                    Prioridad = item.Prioridade.Descripcion,
                    Estado = item.EstadoIncidencia.Descripcion,
                    Tecnico = item.Identificador,
                    Comentario = item.ComentarioCierre,
                    FechaCierre = item.FechaCierre
                };
                lsIncidenciaViewModel.Add(incidencia);
            };

            return View(lsIncidenciaViewModel.OrderByDescending(x => x.IncidenciaId));
        }
       
    }
}