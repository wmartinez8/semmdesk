﻿using SemmDesk.Web.Controllers;
using SemmDesk.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SemmDesk.Web.Repositorios
{
    public class FiltroLogin : ActionFilterAttribute
    {
        private UsuarioViewModel usuario;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                base.OnActionExecuting(filterContext);
                usuario = (UsuarioViewModel)HttpContext.Current.Session["Usuario"];
                if (usuario == null)
                {
                    if (filterContext.Controller is LoginController == false)
                    {
                        filterContext.HttpContext.Response.Redirect("~/Login/Login");
                    }
                }
            }
            catch (Exception)
            {

                filterContext.Result = new RedirectResult("~/Login/Login");
            }
        }
    }
}