﻿namespace SemmDesk.Web.Repositorios
{
    using SemmDesk.DAL.Contexto;
    using SemmDesk.Entities.Models;
    using SemmDesk.Web.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class FiltroAuthorize : ActionFilterAttribute
    {
        private readonly Empleado empleado;
        private readonly Modelo _db;
        private readonly int _idOperacion;

        public FiltroAuthorize(int idOperacion)
        {
            this.empleado = new Empleado();
            this._db = new Modelo();
            this._idOperacion = idOperacion;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            var empleado = (Empleado)HttpContext.Current.Session["Usuario"];
            try
            {
                //Estoy comprando con el departamento de manera brusca porque la estructura
                // de la DB a la que le hice una vista no tiene un campo ROL. La idea era
                // que el controlador lo filtraba segun el RolID
                if (empleado.RolId == (int)EnumTecnicoRol.Usuario)
                {
                    filterContext.Result = new RedirectResult("~/Error/UnAuthorized");
                }
            }
            catch (Exception)
            {

                filterContext.Result = new RedirectResult("~/Error/index");
            }
        }



    }
}