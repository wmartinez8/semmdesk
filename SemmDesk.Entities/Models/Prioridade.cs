namespace SemmDesk.Entities.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Prioridade
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Prioridade()
        {
            Incidencias = new HashSet<Incidencia>();
        }

        [Key]
        public int PrioridadId { get; set; }

        [StringLength(25)]
        public string Descripcion { get; set; }

        [StringLength(25)]
        public string Color { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Incidencia> Incidencias { get; set; }
    }
}
