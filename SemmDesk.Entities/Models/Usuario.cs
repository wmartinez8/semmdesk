namespace SemmDesk.Entities.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Usuario
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Usuario()
        {
            Incidencias = new HashSet<Incidencia>();
        }

        public int UsuarioId { get; set; }

        public int CodigoUsuario { get; set; }

        [StringLength(25)]
        public string Identificador { get; set; }

        public string Nombres { get; set; }

        public int? DepartamentoId { get; set; }

        [StringLength(1)]
        public string Estado { get; set; }

        public int? RolId { get; set; }

        public int? MenuId { get; set; }

        public virtual Departamento Departamento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Incidencia> Incidencias { get; set; }

        public virtual Menu Menu { get; set; }
    }
}
