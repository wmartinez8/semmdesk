namespace SemmDesk.Entities.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("vDepartamentos")]
    public partial class vDepartamento
    {
        [Column(TypeName = "numeric")]
        public decimal? codito { get; set; }

        [Key]
        public int departam { get; set; }

        [StringLength(100)]
        public string descrip { get; set; }
    }
}
