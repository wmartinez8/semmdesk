﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SemmDesk.Entities.Models
{
    [Table("Empleados")]
    public class Empleado
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(name: "id_empleado")]
        public int EmpleadoId { get; set; }

        [StringLength(1)]
        [Column(name: "status")]
        public string Estatus { get; set; }

        [Column(name: "codigo")]
        public int Codigo { get; set; }

        [Column(name: "apellido1")]
        public string Apellido1 { get; set; }

        [Column(name: "apellido2")]
        public string Apellido2 { get; set; }

        [StringLength(152)]
        [Column(name: "nombre")]
        public string Nombres { get; set; }

        //[Column(name: "cedula")]
        //public int? Cedula { get; set; }

        [StringLength(1)]
        [Column(name: "sexo")]
        public string Sexo { get; set; }

        [Column(name: "departam", TypeName = "numeric")]
        public int DepartamentoId { get; set; }

        [Column(name: "telefono1")]
        public string Telefono { get; set; }

        //[Column(name: "celular")]
        //public int? Celular { get; set; }

        [Column(name: "cargo")]
        //[StringLength(35)]
        public string Cargo { get; set; }

        [Column(name: "foto", TypeName = "text")]
        public string Foto { get; set; }

        [Column(name: "dptot")]
        public string Departamento { get; set; }

        [Column(name: "fecha_nac")]
        public DateTime FechaNacimiento { get; set; }

        [Column(name: "RolId")]
        public int RolId { get; set; }

        [Column(name: "clave")]
        public string Clave { get; set; }

    }
}
