﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SemmDesk.Entities.Models
{
    public class Area
    {
        public int AreaId { get; set; }
        public string NombreArea { get; set; }
    }
}
