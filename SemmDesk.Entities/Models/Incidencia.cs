
namespace SemmDesk.Entities.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Incidencia
    {
        public int IncidenciaId { get; set; }

        public int CategoriaID { get; set; }

        public int SubCategoriaID { get; set; }

        [StringLength(500)]
        public string Descripcion { get; set; }

        public int? PrioridadID { get; set; }

        public int? EstadoIncidenciaID { get; set; }

        public int EmpleadoId { get; set; }

        public int? TecnicoID { get; set; }

        [StringLength(500)]
        public string ComentarioCierre { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FechaCreacion { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? FechaCierre { get; set; }

        public int UsuarioCreacion { get; set; }

        public int? UsuarioModificacion { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? UltimaModificacion { get; set; }

        public int? TecnicoReAsignadoID { get; set; }

        public int AreaId { get; set; }

        public Categoria Categoria { get; set; }

        public EstadoIncidencia EstadoIncidencia { get; set; }

        public SubCategoria SubCategoria { get; set; }

        public Prioridade Prioridade { get; set; }

        public Tecnico Tecnico { get; set; }

        public Area Area { get; set; }

        public Empleado Empleado { get; set; }
    }
}
