namespace SemmDesk.Entities.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Tecnicos")]
    public partial class Tecnico
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Tecnico()
        {
            Incidencias = new HashSet<Incidencia>();
        }

        public int TecnicoId { get; set; }

        public int CodigoEmpleado { get; set; }

        [Required]
        [StringLength(50)]
        public string Nombres { get; set; }

        [Required]
        [StringLength(25)]
        public string Identificador { get; set; }

        public int? RolId { get; set; }

        public string Clave { get; set; }


        public int AreaId { get; set; }

        public ICollection<Incidencia> Incidencias { get; set; }

        public Role Role { get; set; }
        public Area Area { get; set; }
    }
}
