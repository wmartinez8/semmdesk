namespace SemmDesk.Entities.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vUsuario
    {
        [StringLength(1)]
        public string status { get; set; }

        public int? codigo { get; set; }

        [StringLength(51)]
        public string Identificador { get; set; }

        [StringLength(152)]
        public string Nombres { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? departam { get; set; }

        [Column(TypeName = "text")]
        public string foto { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id_empleado { get; set; }

        [StringLength(35)]
        public string cargo { get; set; }

        public int? Rol { get; set; }
    }
}
